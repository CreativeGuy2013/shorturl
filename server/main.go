package main

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/creativeguy2013/shorturl/server/api"
	"gitlab.com/creativeguy2013/shorturl/server/ui"
)

var cfg config

func main() {
	cfg = loadConfig()
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { http.Redirect(w, r, cfg.DefaultRedirect, 301) })
	r.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {})
	r.HandleFunc("/{short}", handleShort)

	apiRouter := r.PathPrefix("/api").Subrouter()
	api.Serve(apiRouter)

	ui.Info("[server] http server started")
	err := http.ListenAndServe(":"+cfg.Port, r)
	if err != nil {
		ui.Error("[server] stopped due to error (" + err.Error() + ")")
	}
	ui.Info("[server] http server stopped")
}

func handleShort(w http.ResponseWriter, r *http.Request) {
	urlVars := mux.Vars(r)
	ui.Info("[shorturl] processing short url with short \"" + urlVars["short"] + "\"")

	shortURL, err := api.URLGet(urlVars["short"])
	if err != nil {
		http.Redirect(w, r, cfg.DefaultRedirect, 307)
		return
	}

	http.Redirect(w, r, shortURL.Long, 301)
}

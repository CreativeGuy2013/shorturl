package api

import (
	"encoding/json"
	"errors"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/creativeguy2013/scribble"
	"github.com/google/uuid"

	"github.com/gorilla/mux"
	"gitlab.com/creativeguy2013/shorturl/server/ui"
)

var db *scribble.Document

func init() {
	var err error
	db, err = scribble.New("db")
	if err != nil {
		ui.Error("[db] couldn't open database (" + err.Error() + ")")
	}
}

//ShortURL defines the correlation between a tag and a long url.
type ShortURL struct {
	Short string `json:"short"`
	Long  string `json:"long"`
}

//URLGet returns the ShortURL object with that short
func URLGet(short string) (shortURL ShortURL, err error) {
	err = db.Collection("shorturls").Document(short).Read(&shortURL)
	return
}

const amountPerPage = 10

//URLGetPage returns the ShortURL object with that short
func URLGetPage(page int) (shortURLs []ShortURL, lastPage int, err error) {
	docs, err := db.Collection("shorturls").GetAllDocuments()
	if err != nil {
		return nil, 0, err
	}

	var start, end int
	if page*amountPerPage < len(docs) {
		if len(docs)-page*amountPerPage <= amountPerPage {
			start = page * amountPerPage
			end = len(docs) % amountPerPage
		} else {
			start = page * amountPerPage
			end = page*amountPerPage + amountPerPage
		}
	}

	lastPage = int(math.Floor(float64(len(docs)) / float64(amountPerPage)))

	shortURLs = make([]ShortURL, end-start)
	for i, doc := range docs[start:end] {
		err = doc.Read(&shortURLs[start+i])
		if err != nil {
			return nil, 0, err
		}
	}

	return
}

//URLCreate adds a new ShortURL to database
func URLCreate(shortURL ShortURL) error {
	err := db.Collection("shorturls").Document(shortURL.Short).Read(nil)
	if err == nil {
		ui.Warning("[db] shortURL already exists")
		return errors.New("shortURL already exists")
	}

	err = db.Collection("shorturls").Document(shortURL.Short).Write(shortURL)
	if err != nil {
		ui.Warning("[db] error writing new shorturl into database (" + err.Error() + ")")
		return errors.New("error writing new shorturl into database")
	}

	return nil
}

//TokenCheck checks if the API token is still valid
func TokenCheck(token string) (valid bool) {
	var t time.Time
	err := db.Collection("apitokens").Document(token).Read(&t)
	if err == nil {
		return t.After(time.Now())
	}

	return false
}

//TokenCreate makes a new API token that is valid until a certain time
func TokenCreate(validUntil time.Time) (token string, err error) {
	token = uuid.New().String()
	err = db.Collection("apitokens").Document(token).Write(validUntil)
	if err != nil {
		ui.Warning("[db] error writing new apitoken into database (" + err.Error() + ")")
		err = errors.New("error writing new apitoken into database")
		return
	}

	return
}

/*
	Actuall handling of /api endpoint
*/

//Serve the api for the frontend
func Serve(r *mux.Router) {
	v1 := r.PathPrefix("/v1").Subrouter()
	v1.Use(v1TokenMiddleware)

	v1.HandleFunc("/ping", v1Ping)
	v1.HandleFunc("/shorturls", v1ShortURLGetAll).Methods("GET")
	v1.HandleFunc("/shorturls", v1ShortURLCreate).Methods("POST")
	v1.HandleFunc("/shorturls/{short}", v1ShortURLGet).Methods("GET")
}

func v1TokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auths := strings.Split(r.Header.Get("Authorization"), " ")
		if len(auths) == 2 && auths[0] == "Bearer" && TokenCheck(auths[1]) {
			next.ServeHTTP(w, r)
			return
		}

		http.Error(w, "api token not valid or Authorization header not set correctly", http.StatusUnauthorized)
		return
	})
}

func v1Ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("valid"))
}

func v1ShortURLGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	shortURL, err := URLGet(vars["short"])
	if err != nil {
		http.Error(w, "shortURL doesn't exist", http.StatusNotFound)
		return
	}

	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "    ")
	err = encoder.Encode(shortURL)
	if err != nil {
		http.Error(w, "internal server error (1)", http.StatusInternalServerError)
		return
	}
}

func v1ShortURLGetAll(w http.ResponseWriter, r *http.Request) {
	p := r.FormValue("page")
	if p == "" {
		p = "0"
	}

	page, err := strconv.Atoi(p)
	if err != nil {
		http.Error(w, "page query tag is not an int", http.StatusBadRequest)
		return
	}

	shortUrls, lastPage, err := URLGetPage(page)
	if err != nil {
		ui.Debug(err.Error())
		http.Error(w, "internal server error (1)", http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "    ")
	err = encoder.Encode(map[string]interface{}{
		"shortUrls": shortUrls,
		"lastPage":  lastPage,
	})

	if err != nil {
		http.Error(w, "internal server error (2)", http.StatusInternalServerError)
		return
	}
}

func v1ShortURLCreate(w http.ResponseWriter, r *http.Request) {
	var shortURL ShortURL
	encoder := json.NewDecoder(r.Body)
	err := encoder.Decode(&shortURL)
	if err != nil {
		http.Error(w, "json is not formated properly", http.StatusBadRequest)
		return
	}

	if shortURL.Short == "" || shortURL.Long == "" {
		http.Error(w, "short or long urls are not set", http.StatusBadRequest)
		return
	}

	err = URLCreate(shortURL)
	if err != nil {
		http.Error(w, "internal server error (1)", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

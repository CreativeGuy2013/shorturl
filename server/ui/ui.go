package ui

import (
	"fmt"
	"os"
	"time"

	"github.com/fatih/color"
)

var logFile *os.File

func init() {
	var err error
	logFile, err = os.Create("shorturl.log")
	if err != nil {
		panic("could not create log file")
	}
}

//Debug output to the console
func Debug(data string) {
	color.Blue("[%v] [DEBUG] %v\n", time.Now().Format(time.StampMilli), data)
	color.New(color.FgBlue).Fprintf(logFile, "[%v] [DEBUG] %v", time.Now().Format(time.StampMilli), data)
	fmt.Fprintln(logFile, "")
}

//Info output to the console
func Info(data string) {
	color.White("[%v] [INFO] %v\n", time.Now().Format(time.StampMilli), data)
	fmt.Fprintf(logFile, "[%v] [INFO] %v", time.Now().Format(time.StampMilli), data)
	fmt.Fprintln(logFile, "")
}

//Warning output to the console
func Warning(data string) {
	color.Yellow("[%v] [WARNING] %v\n", time.Now().Format(time.StampMilli), data)
	fmt.Fprintf(logFile, "[%v] [WARNING] %v", time.Now().Format(time.StampMilli), data)
	fmt.Fprintln(logFile, "")
}

//Error output to the console
func Error(data string) {
	color.Red("[%v] [ERROR] %v\n", time.Now().Format(time.StampMilli), data)
	color.New(color.FgRed).Fprintf(logFile, "[%v] [ERROR] %v", time.Now().Format(time.StampMilli), data)
	fmt.Fprintln(logFile, "")
	panic(data)
}

import React from 'react';
import { Link } from 'react-router-dom';

import {
    Collapse,
    Navbar as NavBar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

export class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <NavBar color="primary" dark expand="md">
                <NavbarBrand href="/">Short URL</NavbarBrand>
                <NavbarToggler onClick={() => this.toggle()} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>Servers</DropdownToggle>
                            <DropdownMenu right>
                                {this.props.servers.length > 0? (
                                    this.props.servers.map((server) => {
                                        return <DropdownItem tag={Link} to={`/server/${server.id}`}>{server.domain}</DropdownItem>
                                    })
                                ) : (
                                        <DropdownItem disabled><i>No servers...</i></DropdownItem>
                                    )}
                                <DropdownItem divider />
                                <DropdownItem tag={Link} to={`/`}>Add a server</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </NavBar>
        )
    }
}
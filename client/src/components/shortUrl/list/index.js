import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import { Container, Table, Button } from 'reactstrap';

export class ShortURLList extends React.Component {
    constructor(props) {
        super(props)


        this.state = {
            data: [],
            lastPage: 1
        }
    }

    componentDidMount() {
        axios.get(`${this.props.server.domain}/api/v1/shorturls?page=${this.props.page - 1}`, {
            headers: {
                "Authorization": `Bearer ${this.props.server.token}`
            }
        }).then(resp => {
            console.log(resp)
            if (resp.status === 200) {
                this.setState({
                    data: resp.data.shortUrls,
                    lastPage: resp.data.lastPage
                })
            }
        }).catch(err => {
            console.error(err)
        })
    }

    render() {
        return (
            <Container>
                <br />
                <h2>{this.props.server.domain}</h2>
                <p>View existing short urls below or <Link to={`/server/${this.props.server.id}/add`}>add a new short url</Link>.</p>
                <Table>
                    <thead>
                        <tr>
                            <th>Short URL</th>
                            <th>Long URL</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.length !== 0 ? (
                            this.state.data.map(d => (
                                <tr>
                                    <td><a href={`${this.props.server.domain}/${d.short}`}>{this.props.server.domain}/{d.short}</a></td>
                                    <td><a href={d.long}>{d.long}</a></td>
                                </tr>
                            ))
                        ) : (
                                <tr>
                                    <td rowSpan="2">Loading...</td>
                                </tr>
                            )}
                    </tbody>
                </Table>
            </Container>
        )
    }
}
import React from 'react';
import axios from 'axios';
import QRReader from 'react-qr-scanner'

import { Redirect } from 'react-router-dom';

import { Container, Input, Button, Form, FormGroup, Label } from 'reactstrap';

const previewStyle = {
    width: "100%",
}

export class ServerAdd extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            domain: getParameterByName("domain"),
            token: getParameterByName("token"),
            done: ""
        }
    }

    submit(e) {
        e.preventDefault()

        axios.get(`${this.state.domain}/api/v1/ping`, {
            headers: {
                "Authorization": `Bearer ${this.state.token}`
            }
        }).then(resp => {
            if (resp.data === "valid") {
                console.log(resp.data)
                var id = this.props.serverAdd(this.state.token, this.state.domain)
                this.setState({done: id})
            }
        }).catch(err => {
            console.error(err)
        })
    }

    handleScan(data) {
        if (data !== null) {
            this.setState({
                domain: getParameterByName("domain", data),
                token: getParameterByName("token", data)
            })
        }
    }

    handleError(err) {
        alert(err)
        console.error(err)
    }

    render() {
        if (this.state.done) {
            return <Redirect to={`/server/${this.state.done}`} />
        }

        return (
            <Container style={{ textAlign: "center" }}>
                <br />
                <h3>Add a server</h3>
                {this.state.qrEnabled ? (
                    <QRReader
                        delay={10}
                        facingMode="rear"
                        style={previewStyle}
                        onError={err => this.handleError(err)}
                        onScan={data => this.handleScan(data)}
                    />
                ) : (
                        <p>Already added the server to another device? <a onClick={() => this.setState({ qrEnabled: true })} href="#qr">Scan the QR code.</a></p>
                    )}
                <Form style={{ margin: "0 auto" }} onSubmit={(e) => this.submit(e)}>
                    <FormGroup>
                        <Label for="exampleEmail">Domain</Label>
                        <Input type="domain" name="domain" id="domain" placeholder="https://go.antipy.com" value={this.state.domain} onChange={e => this.setState({ domain: e.target.value })} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleEmail">API Token</Label>
                        <Input type="text" name="api_token" id="api_token" placeholder="d52af2d6-309a-b253-458a-2443b324e6f9" value={this.state.token} onChange={e => this.setState({ token: e.target.value })} />
                    </FormGroup>
                    <Button>Add</Button>
                </Form>
            </Container>
        )
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

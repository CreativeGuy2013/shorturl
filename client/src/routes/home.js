import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import { ServerSelect } from '../components/server/select';
import { ServerAdd } from '../components/server/add';

export class HomeRoute extends React.Component {
    render() {
        return (
            <>
                <Switch>
                    <Route path="/add" exact render={r => <ServerAdd servers={this.props.servers} serverAdd={(token, domain) => this.props.serverAdd(token, domain)} />} />
                    <Route path="/" exact render={r => <ServerSelect servers={this.props.servers} serverRemove={(id) => this.props.serverRemove(id)} />} />
                    <Route render={r => <Redirect to="/" />}/>
                </Switch>
            </>
        )
    }
}
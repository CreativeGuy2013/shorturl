import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import shortid from "shortid";

import { Navbar } from './components/navbar';

import { ServerRoute } from './routes/server';
import { HomeRoute } from './routes/home';

class App extends React.Component {
  constructor(props) {
    super(props)

    var lsservers = localStorage.getItem("servers")
    this.state = {
      servers: JSON.parse(lsservers ? lsservers : "[]"),
      changed_servers: false
    }
  }

  serverAdd(token, domain) {
    var servers = this.state.servers

    if (servers.find(el => (el.token === token || el.domain === domain)) !== undefined) {
      return ""
    }

    var id = shortid.generate()
    servers.push({
      domain,
      token,
      id
    })

    this.setState({
      servers,
      changed_servers: true
    })

    return id
  }

  serverRemove(id) {
    var servers = this.state.servers

    servers.filter(el => el.id !== id)

    this.setState({
      servers,
      changed_servers: true
    })
  }

  render() {
    console.log(this.state)
    if (this.state.changed_servers) {
      localStorage.setItem("servers", JSON.stringify(this.state.servers))
      this.setState({ changed_servers: false })
    }

    return (
      <>
        <Navbar servers={this.state.servers}/>
        <Switch>
          <Route path="/server/:server" render={(r) => {
            var server = this.state.servers.find(el => el.id === r.match.params.server)
            if (server === undefined) {
              return <Redirect to="/" />
            } else {
              return <ServerRoute server={server} serverRemove={() => this.serverRemove(server.id)} />
            }
          }} />
          <Route path="/add" exact render={() => <HomeRoute servers={this.state.servers} serverAdd={(token, domain) => this.serverAdd(token, domain)} serverRemove={(id) => this.serverRemove(id)} />} />
          <Route path="/" exact render={() => <HomeRoute servers={this.state.servers} serverAdd={(token, domain) => this.serverAdd(token, domain)} serverRemove={(id) => this.serverRemove(id)} />} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </>
    );
  }
}

export default App;
